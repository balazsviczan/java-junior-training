package com.epam.alltogether;

import org.junit.Test;

public class ValidatorTest {
	
	@Test(expected=IllegalArgumentException.class)
	public void testValidateNotNullWorksProperlyForNull(){
		//GIVEN

		//WHEN
		
		//THEN
		Validator.validateNotNull(null);
	}

}
