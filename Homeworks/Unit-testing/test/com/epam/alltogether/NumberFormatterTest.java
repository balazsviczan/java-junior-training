package com.epam.alltogether;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NumberFormatterTest {
	
	public static NumberFormatter underTest;
	
	@Before
	public void setUp(){
		underTest = new NumberFormatter();
	}

	@Test
	public void testFormat(){
		//GIVEN

		//WHEN
		String expected = "[5]";
		Integer number = 5;
		
		//THEN
		Assert.assertEquals(expected, underTest.format(number));
		
	}
}
