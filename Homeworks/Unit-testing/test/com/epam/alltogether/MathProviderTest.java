package com.epam.alltogether;

import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MathProviderTest {
	
	public static String EXPECT_SUM_RESULT = "[20]";
	
	public MathProvider underTest; 
	
	public AbstractCalculator calc;

	@Before
	public void setUp(){
		underTest = new MathProvider();
		calc = EasyMock.createStrictMock(AbstractCalculator.class);
		underTest.setCalc(calc);
	}
	
	@Test
	public void testSum(){
		//GIVEN
		List<Integer> numbers = new ArrayList<>();
		numbers.add(1);
		EasyMock.expect(calc.calculate(0, 1)).andReturn(20);
		EasyMock.replay(calc);
		//WHEN
		String actual = underTest.sum(numbers);
		//THEN
		EasyMock.verify(calc);
		Assert.assertEquals(EXPECT_SUM_RESULT, actual);
		
	}
}
