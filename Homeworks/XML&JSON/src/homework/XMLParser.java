package homework;

import java.awt.Color;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

public class XMLParser {
	
	PrintWriter writer;
	XMLStreamReader reader;
	Map <String, String> colors = new HashMap<>();
	
	
	public static void main(String[] args) throws XMLStreamException, IOException {
		XMLParser app = new XMLParser();
		app.processWithStAxStreamReader();
	}
	
	private void processWithStAxStreamReader() throws XMLStreamException, IOException {
		
		init();
		
		writer.append("<!DOCTYPE>\n<html>\n<head>\n</head>\n<body>\n\t<svg width=\"1700\" height=\"800\">");
		writer.flush();

		
		createCircles();
		
		
		writer.append("\n\t</svg>\n</body>\n</html>");
		writer.flush();
		writer.close();
		
		Desktop.getDesktop().browse(new File("index.html").toURI());
	}

	private void createCircles() throws NumberFormatException, XMLStreamException {
		
		double latitude=0;
		double longitude=0;
		String state="";
		
		while (reader.hasNext()) {
			int event = reader.next();
			if (event == XMLStreamConstants.START_ELEMENT) {
				if ("latitude".equals(reader.getLocalName())) {
					latitude = Double.parseDouble(reader.getElementText());
				}
				if ("longitude".equals(reader.getLocalName())) {
					longitude = Double.parseDouble(reader.getElementText());
				}
				if ("state".equals(reader.getLocalName())) {
					state = reader.getElementText();
				}
				
				
			} else if(event == XMLStreamConstants.END_ELEMENT){
				if ("record".equals(reader.getLocalName())) {
					if(latitude!=0 && longitude!=0){
						String color;
						if(colors.containsKey(state)){
							color = colors.get(state);
						} else {
							color = randomColor();
							colors.put(state, color);
						}
							
						writer.append("\n\t\t<circle cy=\"" + (800 - (latitude *10)) + "\" cx=\"" + ((longitude * 10)+1300) + "\" fill=\"" + color + "\" r=\"1\"></circle> ");
					}
				}
			}
		}
		
	}

	private void init() throws FileNotFoundException, UnsupportedEncodingException, XMLStreamException, FactoryConfigurationError {
		File htmlFile = new File("index.html");
		writer = new PrintWriter(htmlFile, "UTF-8");
		InputStream xml = getClass().getClassLoader().getResourceAsStream("homework/zip-full.xml");
		Source xmlFile = new StreamSource(xml);
		reader = XMLInputFactory.newInstance().createXMLStreamReader(xmlFile);
	}

	private String randomColor() {
		Random rand = new Random();
		int r = rand.nextInt(255);
		int g = rand.nextInt(255);
		int b = rand.nextInt(255);
		Color color = new Color(r, g, b);
		String hex = Integer.toHexString(color.getRGB() & 0xffffff);
		return "#"+hex;
	}


	
}
