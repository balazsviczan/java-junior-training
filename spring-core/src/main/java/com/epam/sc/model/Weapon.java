package com.epam.sc.model;

public class Weapon {

	private String name;
	private int dmg;

	public Weapon(String name, int dmg) {
		super();
		this.name = name;
		this.dmg = dmg;
	}

	public String getName() {
		return name;
	}

	public int getDmg() {
		return dmg;
	}

	@Override
	public String toString() {
		return name + ", dmg=" + dmg;
	}

	
}
