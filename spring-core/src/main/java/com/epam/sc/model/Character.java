package com.epam.sc.model;

public class Character {

	private String name;
	private int hp;
	private Weapon weapon;

	public Character(String name, int hp) {
		super();
		this.name = name;
		this.hp = hp;
		setWeapon(new Weapon(name+"'s fist", 1));
	}
	
	public Character(String name, int hp, Weapon weapon) {
		super();
		this.name = name;
		this.hp = hp;
		this.weapon = weapon;
	}

	public String getName() {
		return name;
	}

	public int getHp() {
		return hp;
	}

	public Weapon getWeapon() {
		return weapon;
	}

	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}

	@Override
	public String toString() {
		return name + " hp=" + hp + ", weapon=[" + weapon + "]";
	}

}
