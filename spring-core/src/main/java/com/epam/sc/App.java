package com.epam.sc;

import com.epam.sc.model.Character;

import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {

		final ApplicationContext context= 
				new ClassPathXmlApplicationContext("beans.xml", "beans2.xml");
		Map<String, Character> characters = context.getBeansOfType(Character.class);
		final Character character = context.getBean("ogre", Character.class);
		System.out.println(character);
	}
	
}
